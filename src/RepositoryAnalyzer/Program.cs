﻿namespace RepositoryAnalyzer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LibGit2Sharp;

    class Program
    {
        private static Dictionary<string, int> filesFrequency = new Dictionary<string, int>();

        static void Main(string[] args)
        {
            using (var repo = new Repository(args[0]))
            {
                foreach (var log in repo.Commits.QueryBy(new CommitFilter() { SortBy = CommitSortStrategies.Time }))
                {
                    if (log.Parents.Any())
                    {
                        var oldTree = log.Parents.First().Tree;
                        var changes = repo.Diff.Compare<TreeChanges>(oldTree, log.Tree);
                        foreach (var change in changes)
                        {
                            UpdateFileFrequency(change.Path);
                        }
                    }
                    else
                    {
                        foreach (var entry in log.Tree)
                        {
                            CountUpdatedFilesFrequency(entry);
                        }
                    }
                }
            }

            foreach (var f in filesFrequency.Select(f => new { Frequency = f.Value, File = f.Key })
                                            .OrderByDescending(f => f.Frequency))
            {
                Console.WriteLine($"{f.Frequency}t{f.File}");
            }

            Console.ReadKey();
        }

        private static void CountUpdatedFilesFrequency(TreeEntry file)
        {
            if (file.Mode == Mode.Directory)
            {
                foreach (var child in (file.Target as Tree))
                {
                    CountUpdatedFilesFrequency(child);
                }
            }
            else
                UpdateFileFrequency(file.Path);
        }

        private static void UpdateFileFrequency(string file)
        {
            int frequency = 0;
            filesFrequency.TryGetValue(file, out frequency);
            frequency++;
            filesFrequency[file] = frequency;
        }
    }
}
